const mongoClient = require('mongodb').MongoClient;
const config = require("./config");
    
module.exports = async function() {
    let client;
    try {
        // Connect to mongoDB
        client = await mongoClient.connect(config.mongo_prod, {useUnifiedTopology: true});
        let db = client.db('urup');
        let dCollection = db.collection('widget_groups');
        // read in widget_groups per organisation and project
        //let result = await dCollection.find({}).project({_id:0, organisation:1, project:1, widget_id:1, widget_name:1, widget_group:1, widget_type:1}).toArray(); 
        let result = await dCollection.find({}).toArray(); 
        return result;

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    } 
}

