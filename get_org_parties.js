const mongoClient = require('mongodb').MongoClient;
const config = require("./config");

module.exports = async function() {
    let client;
    try {
        // Connect to mongoDB
        client = await mongoClient.connect(config.mongo_prod, {useUnifiedTopology: true});
        let db = client.db('urup');

        dCollection = db.collection('org_parties');
        //result = await dCollection.find({}).project({_id:0, organisation:1, project:1, emp_code:1, full_name:1, division:1}).toArray();
        result = await dCollection.find({}).toArray();
        return result;

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    } 
}
