'use strict';

const Hapi = require('@hapi/hapi');
const config = require('./config');
var dataCache = require("./data_cache");
var getWidgetGroups = require("./get_widget_groups");
var getOrgElements = require("./get_org_elements");
var getOrgParties = require("./get_org_parties");

const init = async () => {

    const server = Hapi.server({
        port: config.server.port,
        host: config.server.host
    });

    const widgetGroupsCache = new dataCache(getWidgetGroups, config.widget_groups_ttl);
    const orgElementsCache = new dataCache(getOrgElements, config.org_elements_ttl);
    const orgPartiesCache = new dataCache(getOrgParties, config.org_parties_ttl);


    server.route({
        method: 'GET',
        path: '/api/cache/widget_groups',
        handler: async function(request, h) {
                //let auth = await checkToken(request);

                //if (!auth.authorized) return ({success: false, message: 'Invalid token provided!'});

                let ret = await widgetGroupsCache.getData();

                if (request.query) {
                    let org = request.query.organisation;
                    let project = request.query.project;

                    if (org) {
                        ret = ret.filter(o => o.organisation === org);
                    }
                    if (project) {
                        ret = ret.filter(o => o.project === project);
                    }
                    if (request.query.widget_id) {
                        let wid = request.query.widget_id;
                        ret = ret.filter(o => o.widget_id === wid);
                    }
                    if (request.query.widget_group) {
                        let wg = request.query.widget_group;
                        ret = ret.filter(o => o.widget_group === wg);
                    }

                }
                   
                return ({   
                    success: true,
                    data: ret
                });    

        }
    });

    server.route({
        method: 'GET',
        path: '/api/cache/org_elements',
        handler: async function(request, h) {
                //let auth = await checkToken(request);

                //if (!auth.authorized) return ({success: false, message: 'Invalid token provided!'});
                let ret = await orgElementsCache.getData();

                if (request.query) {
                    let org = request.query.organisation;
                    let project = request.query.project;

                    if (request.query.emp_code) {
                        let ec = request.query.emp_code;
                        ret = ret.filter(o => o.emp_code === ec);
                    }
                    if (request.query.division) {
                        let div = request.query.division;
                        ret = ret.filter(o => o.division === div);
                    }
                    if (request.query.store_id) {
                        let sid = request.query.store_id;
                        ret = ret.filter(o => o.store_id === Number(sid));
                    }
                }
                    
                return ({   
                    success: true,
                    data: ret
                });    

        }
    });

    server.route({
        method: 'GET',
        path: '/api/cache/org_parties',
        handler: async function(request, h) {
                //let auth = await checkToken(request);

                //if (!auth.authorized) return ({success: false, message: 'Invalid token provided!'});
                let ret = await orgPartiesCache.getData();

                if (request.query) {
                    let org = request.query.organisation;
                    let project = request.query.project;

                    if (request.query.emp_code) {
                        let ec = request.query.emp_code;
                        ret = ret.filter(o => o.emp_code === ec);
                    }
                    if (request.query.division) {
                        let div = request.query.division;
                        ret = ret.filter(o => o.division === div);
                    }
                    if (request.query.store_id) {
                        let sid = request.query.store_id;
                        ret = ret.filter(o => o.store_id === Number(sid));
                    }
                }
                    
                return ({   
                    success: true,
                    data: ret
                });    

        }
    });

    server.route({
        method: 'POST',
        path: '/api/cache/refresh',
        handler: async function(request, h) {
            if (request.payload) {
                let org = request.payload.organisation;
                let cache = request.payload.cache;
                let ret;

                if (cache === 'org_elements') {
                    ret = await orgElementsCache.resetCache();
                }
                if (cache === 'org_parties') {
                    ret = await orgPartiesCache.resetCache();
                }
                if (cache === 'widget_groups') {
                    ret = await widgetGroupsCache.resetCache();
                }
                return ({   
                    success: true,
                    data: ret
                });    


            }
        }

    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    //process.exit(1);
});

init();
