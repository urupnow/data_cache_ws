const mongoClient = require('mongodb').MongoClient;
const config = require("./config");

module.exports = async function() {
    let client;
    try {
        // Connect to mongoDB
        client = await mongoClient.connect(config.mongo_prod, {useUnifiedTopology: true});
        let db = client.db('urup');

        dCollection = db.collection('org_elements');
        result = await dCollection.find({}).toArray();
        return result;

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    } 
}
